<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Status
 * @package App\Models
 * @version September 11, 2020, 9:06 am UTC
 *
 * @property string $label
 * @property string $code
 */
class Status extends Model
{
    use SoftDeletes;

    public $table = 'statuses';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'label',
        'code'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'label' => 'string',
        'code' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'label' => 'required|max:50',
        'code' => 'required|max:3'
    ];


}
