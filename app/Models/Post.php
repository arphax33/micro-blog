<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Post
 * @package App\Models
 * @version September 11, 2020, 9:10 am UTC
 *
 * @property User
 * @property string $title
 * @property string $content
 * @property string $author
 */
class Post extends Model
{
    use SoftDeletes;

    public $table = 'posts';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'content',
        'author',
        'category',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'content' => 'string',
        'author' => 'integer',
        'category' => 'integer',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required|max:50',
        'content' => 'required|max:255',
        'author' => 'required|exists:User',
        'category' => 'required|exists:Category',
        'status' => 'required|exists:Status'
    ];


    /**
     * Configure the User relationship
     */
    public function author()
    {
        return $this->hasOne('App\Models\User', 'author');
    }

    /**
     * Configure the Category relationship
     */
    public function category()
    {
        return $this->hasOne('App\Models\Category', 'category');
    }

    /**
     * Configure the Category relationship
     */
    public function status()
    {
        return $this->hasOne('App\Models\Status', 'status');
    }
}
