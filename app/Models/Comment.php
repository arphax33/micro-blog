<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Comment
 * @package App\Models
 * @version September 11, 2020, 9:09 am UTC
 *
 * @property string $content
 */
class Comment extends Model
{
    use SoftDeletes;

    public $table = 'comments';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'content',
        'author'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'content' => 'string',
        'author' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'content' => 'required|max:255',
        'author' => 'required|exists:User',
        'post' => 'required|exists:Post'
    ];

    /**
     * Configure the Category relationship
     */
    public function author()
    {
        return $this->hasOne('App\Models\User', 'author');
    }

    /**
     * Configure the Category relationship
     */
    public function post()
    {
        return $this->hasOne('App\Models\Post', 'post');
    }
}
