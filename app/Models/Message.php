<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Message
 * @package App\Models
 * @version September 11, 2020, 9:02 am UTC
 *
 */
class Message extends Model
{
    use SoftDeletes;

    public $table = 'messages';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'content',
        'author'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'content' => 'string',
        'author' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        "content" => "required|max:255",
        "author" => "required|exists:User"
    ];

    /**
     * Configure the User relationship
     */
    public function author()
    {
        return $this->hasOne('App\Models\User', 'author');
    }
}
