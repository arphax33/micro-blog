<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->middleware('verified');


Route::resource('messages', 'MessageController');

Route::resource('categories', 'CategoryController');

Route::resource('statuses', 'StatusController');

Route::resource('documents', 'DocumentController');

Route::resource('comments', 'CommentController');

Route::resource('posts', 'PostController');